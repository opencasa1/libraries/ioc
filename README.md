# IOC

Simple container to hold dependencies and register factories.


# Install

```bash
$ npm install --save @opencasa/ioc
```

# Usage

```javascript
const IOC = require('@opencasa/ioc');
const container = new IOC();
```


# API

## container.add(dependencyName, dependency);

Adding a new dependency to the container is straightforward like this example below.

```javascript
  const cat = {
    name: 'Teo',
    age: 4
  };

  container.add('cat', cat);
```

If you add another dependency with the same name, the last dependency will replace the former one.

## container.get(dependencyName)

Getting a stored dependency in ioc.

```javascript
  const cat = container.get('cat');
  console.log('Cat name', cat.name); // Teo
```

By default each dependency stored in is a singleton.
If there is a need to remove the current instance after use, true can be passed
to remove the instance from the cached instances.

```javascript
  const cat = container.get('cat', true); // 1st time there is no issue, it will return cat instance
  const catError = container.get('cat'); // this will throw an error, due there is no cat instance on cache
```


## container.register(dependencyName, factory,[factoryDepencies])

Having a dependency already created is not always possible. To solve this issue, you can use a factory to create your dependency instead.

```javascript
  const makeCat = () => {
    return {
      name: 'Teo',
      age: 4
    };
  };

  constainer.register('cat', makeCat);
```

Additionally, if your factory needs dependencies which are already registered on container, you can refer them directly as follows.

```javascript
  // You will get your dependency list as an object
  const makeCat = ({ owner }) => {
    return {
      name: 'Teo',
      age: 4,
      askForFood() {
        console.log(`hey ${owner.name}, gimme some food`);
      }
    };
  };

  const owner = {
    name: 'Karen'
  };

  container.add('owner', owner);
  container.register('cat', makeCat, ['owner']);


  const theCat = container.get('cat');
  theCat.askForFood(); //hey Karen, gimme some food
```

## container.toDependencyObject()

When the dependency object is needed to be separated from container object, the dependency tree can be retrieved this way:

```javascript
  container.add('cat', cat);
  container.register('dog', () => dog);

  /*
    dependencies = {
      cat: catInstance,
      dog: dogInstance
    }
  */
  const dependencies = container.toDependencyObject();
```
