const tap = require('tap');
const IOContainer = require('./ioc');

tap.test('IOC', (test) => {
  let container;

  test.beforeEach((done) => {
    container = new IOContainer();
    done();
  });

  test.test('adding a dependency into IOC will store it into dependency attr', (t) => {
    const dependency = { message: 'hi' };
    container.add('fooDependency', dependency);
    t.equal(container.get('fooDependency'), dependency);
    t.end();
  });

  test.test('adding a factory into IOC will store it into factory attr', (t) => {
    const factory = () => ({ message: 'hi' });
    container.register('fooFactory', factory);
    t.deepEqual(container.get('fooFactory').message, 'hi');
    t.end();
  });

  test.test('getting an unexisting dependency into dependencies/factories will throw an error', (t) => {
    t.throws(() => container.get('unexistingKey'));
    t.end();
  });

  test.test('getting a factory with inner dependencies will inject it into factory function', (t) => {
    container.add('milk', { color: 'white' });
    container.add('fish', { tasty: true });

    const createCat = ({ milk, fish }) => {
      return {
        name: 'teo',
        drinkMilk() {
          return `yummy the milk is so ${milk.color}`
        },
        eatFish() {
          return `meeeeow, this is tasty: ${fish.tasty}`;
        }
      }
    };

    container.register('catFactory', createCat, ['milk', 'fish']);
    const cat = container.get('catFactory');
    t.equal(cat.drinkMilk(), 'yummy the milk is so white');
    t.equal(cat.eatFish(), 'meeeeow, this is tasty: true');
    t.end();
  });

  test.test('getting to dependency object will create an object with all dependencies inside', (t) => {
    const cat = {
      name: 'Teo',
      age: 4,
      color: 'orange and white',
      shout() {
        return 'meoooow';
      }
    };
    const dog = {
      name: 'Oliver',
      age: 10,
      color: 'brown',
      shout() {
        return 'wooof';
      }
    };
    const cheese = {
      type: 'Cheddar',
      smelly: true
    };

    container.add('cat', cat);
    container.add('dog', dog);
    container.add('cheese', cheese);
    container.register('mouse', ({ cheese }) => {
      return {
        name: 'jerry',
        eat() {
          return `${this.name} is eating a ${cheese.type}`;
        },
        shout() {
          return 'squeaaak';
        }
      };
    }, ['cheese']);

    const dependencies = container.toDependencyObject();
    t.equal(dependencies.cat.name, cat.name);
    t.equal(dependencies.cat.shout(), 'meoooow');
    t.equal(dependencies.dog.shout(), 'wooof');
    t.equal(dependencies.mouse.shout(), 'squeaaak');
    t.equal(dependencies.mouse.eat(), 'jerry is eating a Cheddar');
    t.end();
  });

  test.test('getting a dependency with .get(depName, true) will delete the dependency instance from cache', (t) => {
    const originalMouse = { name: 'jerry' };
    container.add('jerry', originalMouse);
    t.equal(container.get('jerry', true), originalMouse);
    t.throws(() => container.get('jerry'));
    t.end();
  })

  test.end();
});

