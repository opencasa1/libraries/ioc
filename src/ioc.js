class IOContainer {
  constructor() {
    this._dependencies = {};
    this._factories = {};
  }

  get(dependencyName, toPrune = false) {
    if (this._dependencies[dependencyName]) {
      const dependency = this._dependencies[dependencyName];

      if (toPrune) {
        delete this._dependencies[dependencyName];
      }

      return dependency;
    }

    const factoryOptions = this._factories[dependencyName];

    if (!factoryOptions) {
      const errorMessage = `there is no depedency or factory called ${dependencyName}`;
      throw new TypeError(errorMessage);
    }

    const factoryArgs = this._getFactoryArgs(factoryOptions.args);
    const dependency = factoryOptions.factory.call(null, factoryArgs);
    this._dependencies[dependencyName] = dependency;

    return this._dependencies[dependencyName];
  }

  add(name, dependency) {
    this._dependencies[name] = dependency;
  }

  register(name, factory, dependencies) {
    this._factories[name] = {
      factory,
      args: dependencies || []
    };
  }

  toDependencyObject() {
    const objs = {};

    const setDependency = dependencyKey => {
      const dependency = this.get(dependencyKey);
      objs[dependencyKey] = dependency;
    };

    Object.keys(this._dependencies).forEach(setDependency);
    Object.keys(this._factories).forEach(setDependency);

    return objs;
  }

  _getFactoryArgs(args) {
    return args.reduce((dependencies, dependencyName) => {
      dependencies[dependencyName] = this.get(dependencyName);
      return dependencies;
    }, {});
  }
}


module.exports = IOContainer;
